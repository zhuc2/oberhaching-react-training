import { Box, Button, CircularProgress, TextField } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { addBook, getBooks } from "./services/books.service";
import { Book, NewBook } from "./types/BookType.types";

const NEW_BOOK: NewBook = {
  title: "",
  subject: "",
};

type Errors = Partial<NewBook>;

// union type
const STATUS_IDLE = "idle";
const STATUS_SUBMITTING = "submitting";
const STATUS_SUBMITTED = "submitted";
type Status = "idle" | "submitting" | "submitted";

type ManageBookProps = {
  setBooks: React.Dispatch<React.SetStateAction<Book[]>>;
};

export default function ManageBook({ setBooks }: ManageBookProps) {
  const navigate = useNavigate();
  // const [books, setBooks] = useState(BOOKS);
  // const bookState = useState(BOOKS);
  // const bookss = bookState[0];
  // const setBookss = bookState[1];
  const [isSaving, setIsSaving] = useState(false);
  const [book, setBook] = useState(NEW_BOOK);
  const [status, setStatus] = useState<Status>(STATUS_IDLE);
  // const [book, setBook] = useState<NewBook>({ title: "", subject: "" });
  // const [book, setBook] = useState({ title: "", subject: "" } as NewBook);

  // debugger;

  const errors = getErrors();
  function getErrors() {
    const errors: Errors = {};
    const pattern = /^G-[a-zA-Z0-9]{1,10}$/; // /(G-)[a-zA-Z0-9]{5}/;
    if (!book.title) {
      errors.title = "Title is required.";
    } else if (book.title.length < 2) {
      errors.title = "Title should contain at least 2 characters.";
    }
    if (!pattern.test(book.subject) || book.subject.length > 12) {
      // if (!book.subject.match(pattern)) {
      errors.subject = "Pattern is incorrect.";
    }
    // if (!book.subject) {
    //   errors.subject = "Subject is required.";
    // } else if (book.subject.length < 2) {
    //   errors.subject = "Subject should contain at least 2 characters.";
    // }
    return errors;
  }

  const submitBookAdd = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const errorsExist = Object.keys(errors).length > 0;
    if (errorsExist) {
      setStatus(STATUS_SUBMITTED);
      return;
    }

    setStatus(STATUS_SUBMITTING);
    setIsSaving(true);
    addBook(book).then(async (data) => {
      getBooks().then((responseBooks) => {
        setBooks(responseBooks);
      });
      // const newBooks = [...books, data];
      // setBooks((currentBooks)=>[...currentBooks, data]);
      // console.error(data);
      setBook(NEW_BOOK);
      setIsSaving(false);
      setStatus(STATUS_IDLE);
      await new Promise((resolve) => setTimeout(resolve, 1000));
      navigate("/");
    });

    // const newBooks = [
    //   ...books,
    //   {
    //     id: books.length + 1, // HACK: not good
    //     title: book.title,
    //     subject: book.subject,
    //   },
    // ];
    // setBooks(newBooks);
    // setBook(NEW_BOOK);
    // event.target.title.focus();
  };

  const changeNewBook = (field: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      setBook((currentBook) => ({
        // https://beta.reactjs.org/reference/react/useState#updating-state-based-on-the-previous-state
        ...currentBook,
        [field]: event.target.value,
      }));
    };
  };

  const addForm = (
    <form onSubmit={submitBookAdd}>
      <Box>
        <TextField
          id="title"
          label="Title"
          onChange={changeNewBook("title")}
          value={book.title}
          margin="normal"
          error={status === STATUS_SUBMITTED && Boolean(errors.title)}
          helperText={status === STATUS_SUBMITTED && errors.title}
        />
      </Box>
      <Box>
        <TextField
          id="subject"
          label="Subject"
          onChange={changeNewBook("subject")}
          value={book.subject}
          margin="normal"
          error={status === STATUS_SUBMITTED && Boolean(errors.subject)}
          helperText={status === STATUS_SUBMITTED && errors.subject}
        />
      </Box>
      <Button
        type="submit"
        disabled={book.title?.length === 0 || isSaving}
        variant="contained"
      >
        {isSaving ? "Saving..." : "Add"}
      </Button>
      {isSaving && <CircularProgress aria-label="Saving" />}
    </form>
  );

  return (
    <>
      <h1>Manage Book</h1>
      {addForm}
    </>
  );
}
