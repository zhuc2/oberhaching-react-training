import { Box, Button, CircularProgress } from "@mui/material";
import { useSnackbar } from "notistack";
import { Link } from "react-router-dom";
import { deleteBook, getBooks } from "./services/books.service";
import { Book } from "./types/BookType.types";

type BookAppProps = {
  books: Array<Book>;
  setBooks: React.Dispatch<React.SetStateAction<Book[]>>;
  isLoading: boolean;
  isDone: boolean;
};
export default function BookApp(props: BookAppProps) {
  const { enqueueSnackbar } = useSnackbar();
  const { books, setBooks, isLoading, isDone } = props;

  // function renderBook(book: Book) {
  //   return <li key={book.id}>{book.title}</li>;
  // }

  const clickDeleteBook = (
    books: Array<Book>,
    id: number,
    updateBooks: Function
  ) => {
    return (event: React.MouseEvent<HTMLButtonElement>) => {
      deleteBook(id).then((response) => {
        enqueueSnackbar("Delete!", { variant: "success" });
        getBooks().then((data) => {
          updateBooks(data);
        });
      });
      // debugger;
      // console.error(event.target);
      // console.error(event.target.getAttribute("id"));
      // console.error((event.target as Node).id);
      // updateBooks(books.filter((book) => book.id !== id));
    };
  };

  function ListBooks(books: Array<Book>, updateBooks: Function) {
    if (isLoading && !isDone) {
      return (
        <Box>
          <CircularProgress aria-label="Loading books" />
        </Box>
      );
    }
    return books?.length > 0 ? (
      <ol>
        {books.map((book) => {
          return (
            <li key={book.id}>
              {book.title} ({book.id} - {book.subject}){" "}
              <Button
                id={book.id.toString()}
                variant="outlined"
                size="small"
                onClick={clickDeleteBook(books, book.id, updateBooks)}
                aria-label={"Delete " + book.title}
              >
                Delete
              </Button>
            </li>
          );
        })}
      </ol>
    ) : (
      <div>No books.</div>
    );
  }

  return (
    <>
      <h1>Books</h1>
      <div>
        <Link to="/manage">Add Book</Link>
      </div>
      {ListBooks(books, setBooks)}
    </>
  );
}
