import { lazy, Suspense } from "react";
import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import BookQueryList from "./BookQueryList";
import ManageBook from "./ManageBook";
import NavBar from "./NavBar";
import PageNotFound from "./PageNotFound";

const AboutPage = lazy(() => import("./About"));
export default function AppQuery() {
  return (
    <>
      <BrowserRouter>
        <header>
          <NavBar />
        </header>
        <main>
          <Routes>
            <Route path="*" element={<PageNotFound />}></Route>
            <Route
              path="/"
              element={
                <ErrorBoundary fallback={<p>Error in BookApp</p>}>
                  <BookQueryList />
                </ErrorBoundary>
              }
            ></Route>
            <Route
              path="/manage"
              element={<ManageBook setBooks={() => {}} />}
            ></Route>
            <Route
              path="/about"
              element={
                <Suspense fallback={<p>Loading about...</p>}>
                  <AboutPage />
                </Suspense>
              }
            ></Route>
          </Routes>
        </main>
      </BrowserRouter>
    </>
  );
}
