import { z } from "zod";

// interface Book {
//   id: number;
//   title: string;
//   subject: string;
// }
type Book = z.infer<typeof BookSchema>;

type NewBook = Omit<Book, "id">;

// interface NewBook {
//   title: string;
//   subject: string;
// }

const BookSchema = z.object({
  id: z.number(),
  title: z
    .string({
      required_error: "Title is required",
      invalid_type_error: "Title must be a string",
    })
    .min(2),
  subject: z.string().min(2),
});

export { BookSchema };
export type { Book, NewBook };
