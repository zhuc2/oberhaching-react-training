import { Box, Button, CircularProgress } from "@mui/material";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useSnackbar } from "notistack";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useBooks } from "./hooks/useBooks";
import { deleteBook } from "./services/books.service";
import { Book } from "./types/BookType.types";

export default function BookQueryList() {
  const bookQuery = useBooks();
  const queryClient = useQueryClient();
  const deleteBookMutation = useMutation({
    mutationFn: deleteBook,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ["books"] });
      bookQuery.refetch().then(() => {
        enqueueSnackbar("Delete!", { variant: "success" });
        setDeleteDisabled(false);
      });
    },
  });

  const [deleteDisabled, setDeleteDisabled] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const clickDeleteBook = (id: number) => {
    return (event: React.MouseEvent<HTMLButtonElement>) => {
      setDeleteDisabled(true);
      deleteBookMutation.mutate(id);
      // deleteBook(id).then((response) => {
      //   enqueueSnackbar("Delete!", { variant: "success" });
      // });
    };
  };

  function ListBooks() {
    // Type narrowing - Note that bookQuery.data is always defined below.
    if (bookQuery.isLoading || bookQuery.data === undefined) {
      return (
        <Box>
          <CircularProgress aria-label="Loading books" />
        </Box>
      );
    }

    return bookQuery.data?.length > 0 ? (
      <ol>
        {bookQuery.data.map((book: Book) => {
          return (
            <li key={book.id}>
              {book.title} ({book.id} - {book.subject}){" "}
              <Button
                id={book.id.toString()}
                variant="outlined"
                size="small"
                onClick={clickDeleteBook(book.id)}
                aria-label={"Delete " + book.title}
                disabled={deleteDisabled}
              >
                Delete
              </Button>
            </li>
          );
        })}
      </ol>
    ) : (
      <div>No books.</div>
    );
  }

  return (
    <>
      <h1>Books</h1>
      <div>
        <Link to="/manage">Add Book</Link>
      </div>
      {ListBooks()}
      {bookQuery.isRefetching && (
        <p>
          <CircularProgress aria-label="Loading books" />
          Refetching...
        </p>
      )}
    </>
  );
}
