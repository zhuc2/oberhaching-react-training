import ky, { ResponsePromise } from "ky";
import { Book, BookSchema, NewBook } from "../types/BookType.types";

async function getBooks(): Promise<Array<Book>> {
  const response = await ky.get(import.meta.env.VITE_API_BASE_URL + "/books");
  if (!response.ok) {
    throw new Error("Failed to get books.");
  }
  const responseBooksJson = (await response.json()) as Array<Book>; // Array<any>
  const parsedBooks = responseBooksJson.filter((book: any) => {
    try {
      return BookSchema.parse(book);
    } catch (error) {
      console.error("Failed to parse JSON: " + JSON.stringify(book));
    }
  });
  // debugger;
  return parsedBooks;
}

async function addBook(book: NewBook): Promise<Book> {
  const response = await ky.post(import.meta.env.VITE_API_BASE_URL + "/books", {
    json: book,
  });
  const bookString = JSON.stringify(book);
  // await fetch("http://localhost:3001/books", {
  //   method: "POST",
  //   headers: {
  //     "Content-Type": "application/json",
  //   },
  //   body: bookString,
  // });
  // debugger;

  if (!response.ok) {
    throw new Error("Failed to add book: " + bookString);
  }

  const responseBookJson = await response.json();
  try {
    return BookSchema.parse(responseBookJson);
  } catch (error) {
    throw new Error("Failed to parse book in response: " + responseBookJson);
  }
}

async function deleteBook(id: number): Promise<ResponsePromise> {
  const response = await ky.delete(
    import.meta.env.VITE_API_BASE_URL + "/books/" + id
  );
  // const response = await fetch("http://localhost:3001/books/" + id, {
  //   method: "DELETE",
  // });

  // if (!response.ok) {
  //   throw new Error("Failed to delete book: " + id);
  // }

  // const responseJson = await response.json();
  // debugger;
  return response;
}

export { getBooks, addBook, deleteBook };
