import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import About from "./About";
import BookApp from "./BookApp";
import { Book } from "./types/BookType.types";
import ManageBook from "./ManageBook";
import NavBar from "./NavBar";
import PageNotFound from "./PageNotFound";
import { getBooks } from "./services/books.service";
import { ErrorBoundary } from "react-error-boundary";

export default function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [isDone, setIsDone] = useState(false);
  const [books, setBooks] = useState<Book[]>([]);
  // const bookState = useState(BOOKS);
  // const bookss = bookState[0];
  // const setBookss = bookState[1];
  const [error, setError] = useState<Error | null>(null);

  // debugger;

  useEffect(() => {
    getBooks()
      .then((data) => {
        setBooks(data);
      })
      .catch((e) => {
        setError(e);
      })
      .finally(() => {
        setIsLoading(false);
        setIsDone(true);
      });
    // async function fetchBooks() {
    //   const response = await fetch("http://localhost:3001/books");
    //   if (!response.ok) {
    //     throw response;
    //   }
    //   const booksJson = await response.json();
    //   // (await response.json()) as Book[];
    //   const parsedBooks = booksJson.filter((book: any) => {
    //     try {
    //       return BookSchema.parse(book);
    //     } catch (error) {
    //       console.error("Failed to parse JSON: " + JSON.stringify(book));
    //     }
    //   });
    //   setBooks(parsedBooks);
    // }
    // fetchBooks();
  }, [getBooks]);

  if (error) {
    throw new Error("App Error: " + error);
  }

  return (
    <>
      <BrowserRouter>
        <header>
          <NavBar />
        </header>
        <main>
          <Routes>
            <Route path="*" element={<PageNotFound />}></Route>
            <Route
              path="/"
              element={
                <ErrorBoundary fallback={<p>Error in BookApp</p>}>
                  <BookApp
                    books={books}
                    setBooks={setBooks}
                    isLoading={isLoading}
                    isDone={isDone}
                  />
                </ErrorBoundary>
              }
            ></Route>
            <Route
              path="/manage"
              element={<ManageBook setBooks={setBooks} />}
            ></Route>
            <Route path="/about" element={<About />}></Route>
          </Routes>
        </main>
      </BrowserRouter>
    </>
  );
}
