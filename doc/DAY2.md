# Day 2

Note for Day 2.

## Tasks

1. Add a book.
2. Routing.
3. Matierial UI.
4. Implement API and DB.
5. Validation.

## router

Install the package:

```
npm i react-router-dom
```

## Material UI

Install related packages:

```
npm install @mui/material @emotion/react @emotion/styled

npm install @fontsource/roboto

npm install @mui/icons-material
```

## Json Server

Install replated modules:

```
npm i json-server cross-env npm-run-all
```

Start the server:

```
npx json-server --watch db.json
```

Modify `package.json` file to add some scripts to start server with simpler commands.

```
npm run start:api
npm start
```

## zod

Install:

```
npm i zod
```
