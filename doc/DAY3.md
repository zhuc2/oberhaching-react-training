# Day 3

Note for Day 3.

## Tasks

1. HTTP client: `fetch` vs `ky`.
2. Get/POST/DELETE from/to server.
3. Optimize the app using Matierial UI and `useState`.
4. Env variables.
5. Error handling.
6. Snackbar.

## Object clone - Shallow clone vs Deep clone

`...` is shallow clone.
`structuredClone` is deep clone.

https://twitter.com/housecor/status/1620030346552287233

https://www.builder.io/blog/structured-clone

https://developer.mozilla.org/en-US/docs/Web/API/structuredClone

`--deply 2000` can throttle the response time.

## ky

Install:

```
npm i ky
```

## react-error-boundary

Install:

```
npm i react-error-boundary
```

## Env Variables and Modes

https://vitejs.dev/guide/env-and-mode.html

## Snackbar

https://mui.com/material-ui/react-snackbar/

https://react-hot-toast.com/

In order to use `SnackbarProvider`:

```
npm i notistack
```

## Micro Frontends

https://micro-frontends.org/
