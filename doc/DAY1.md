# Day 1

Note for Day 1.

## Tasks

1. Create a React project.
2. List books.
3. Delete a book.
4. Automated test.

## Vite

Create a React project using Vite:

```
npm create vite@latest .
```

Initialize:

```
npm install
```

Start running it in dev mode:

```
npm run dev
```

## Playwright

Install Playwright module:

```
npm init playwright@latest
```

Run a specific test:

```
npx playwright test books
npx playwright test books --debug
npx playwright test books --headed
```
