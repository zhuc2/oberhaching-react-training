# Day 4

Note for Day 4.

## Outline

- React-query & Custom hooks - HTTP calls with caching, retires, etc.
- Lazy loading pages - Split bundle by route
- Next.js - Framework. BFF, Server side rendering, Server components, Routing

- Reusable components - Design, implementation, documentation
- Unit testing
- Form validation - Displaying errors, declaring state, onBlur, Zod

- Storybook - Build components in isolation. Document reusable components.
- Edit Book
- Context / GLobal state and functions

## React-query

Install:

```
npm i @tanstack/react-query
```

## Lazy loading

```
lazy(() => import("component_path"))
```

## Next.js

```
npx create-next-app@latest
# or
npx create-next-app@latest --example-app
```

## State Management

- https://github.com/pmndrs/zustand
- https://jotai.org/
- https://twitter.com/DavidKPiano/status/1353712136372039682

![](https://pbs.twimg.com/media/EslY5HAXUBMGXa8?format=jpg&name=medium)
