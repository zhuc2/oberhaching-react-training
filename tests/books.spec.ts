import test, { expect } from "@playwright/test";

test("should display a list of books, and support adding and deleting a book", async ({
  page,
}) => {
  const host = "http://localhost:5173/";
  const book1 = page.getByText("Essentialism");
  const book2 = page.getByText("sanguo");
  const newBookTitle = "xiyouji";
  const spinnerLoading = page.getByRole("progressbar", {
    name: "Loading books",
  });
  const addButton = page.getByRole("button", { name: "Add" });
  const savingButton = page.getByRole("button", { name: "Saving..." });
  const spinnerSaving = page.getByRole("progressbar", { name: "Saving" });

  await page.goto(host);

  // Spinner should initially display, then hide
  await expect(spinnerLoading).toHaveCount(1);
  await expect(spinnerLoading).toHaveCount(0);

  // test listing books
  await expect(book1).toHaveCount(1);
  await expect(book2).toHaveCount(1);

  // test adding a book
  await page.getByRole("link", { name: "Add Book" }).click();
  await expect(addButton).toBeDisabled();
  await page.getByLabel("Title").fill(newBookTitle);
  await page.getByLabel("Subject").fill("Novel");
  await expect(addButton).toBeEnabled();
  await addButton.click();
  await expect(spinnerSaving).toHaveCount(1);
  await expect(savingButton).toHaveText("Saving...");
  await expect(savingButton).toBeDisabled();
  await expect(spinnerSaving).toHaveCount(0);
  // and the form should be empty
  // await expect(page.getByLabel("Title")).toHaveValue("");
  // await expect(page.getByLabel("Subject")).toHaveValue("");
  // the new book is added successfully
  // await page.getByRole("link", { name: "Home" }).click();
  await expect(page).toHaveURL(host);
  await expect(page.getByText(newBookTitle)).toHaveCount(1);

  // test deleting a book
  await page.getByRole("button", { name: "Delete " + newBookTitle }).click();
  await expect(page.getByText(newBookTitle)).toHaveCount(0);
});
